# Bienvenido al Wiki de las prácticas de Sistemas Informáticos Industriales

Aquí encontrarás los guiones y el código correspondiente a cada una de las prácticas. 

La versión de la Ubuntu instalada en el **laboratorio de Informática** es la 15.04 ([descargar aquí](http://www.ubuntu.com/download/desktop)). Necesitarás tener los siguientes paquetes instalados:

```
git
gitk
freeglut3-dev
cmake
cmake-gui 
```

**Requisito**: Para la realización de las prácticas se requiere tener una cuenta de usuario en **Bitbucket**, donde se vea claramente el nombre y apellidos del alumno.

**Muy importante**: El repositorio de prácticas del alumno debe permanecer **público y accesible durante todo el semestre** para que los profesores evalúen su trabajo. 

# Normas básicas para trabajar en el laboratorio #

Debido a que el ordenador del laboratorio se utiliza por todos los alumnos de la asignatura, es necesario tener cuidado con los archivos de prácticas y hacer un uso adecuado.

1. El primer paso debe ser crear un directorio personal de trabajo durante tu sesión (por ejemplo, con tu número de matrícula o nombre).
2. Debes establecer la identidad para trabajar con Bitbucket:
```
$git config –global user.name “USUARIO"
$git config –global user.email "EMAIL_BITBUCKET"
```
Cuando termines tu sesión, acuérdate de borrar tu directorio de trabajo y la identidad de Bitbucket (archivo /home/sii/.gitconfig).

# Práctica 1: Entorno de desarrollo bajo Linux #

En esta primera práctica se introduce el entorno de desarrollo de aplicaciones en C/C++ en un sistema operativo **Linux**, utilizando la herramienta **CMake** para la generación del ejecutable y empleando **BitBucket** como sistema de control de versiones Git.

En esta práctica se proporciona un código C++ de una aplicación gráfica utilizando las librerías OpenGL y GLUT. La aplicación es el juego del tenis que funciona en modo local para dos jugadores, en el que utilizan el mismo teclado y pantalla.

El primer paso es hacer un "Fork" del repositorio creado para las prácticas de la asignatura. Un “Fork” es una copia de un repositorio. Permite trabajar independientemente en un repositorio sin afectar al proyecto original.

Para ello, navega al repositorio original (IMPORTANTE: accede al de tu **profesor de prácticas**): 

* Grupos de prácticas de Ángel Rodríguez:
[https://bitbucket.org/arodrifi/sii ](https://bitbucket.org/arodrifi/sii ) 

* Grupos de prácticas de Raquel Cedazo:
[https://bitbucket.org/rcedazo/sii ](https://bitbucket.org/rcedazo/sii ) 

y desde la interfaz web, en el botón del menú de la izquierda "Actions", selecciona la opción "Fork". Aparecerá un formulario donde debes completar (se requiere autenticación en Bitbucket):

* El nombre del nuevo repositorio debe llamarse **sii-DDDDD** (siendo DDDDD el número de matrícula)
* El nivel de acceso debe ser **"público"**.

Tras esto, pulsa en "Fork repository".

El siguiente paso es crear una copia en local para trabajar, para ello se hace un "Clone" del repositorio creado:

```
$ git clone https://USUARIO_ACTUAL@bitbucket.org/USUARIO_PROPIETARIO/sii-DDDDD.git
```
donde USUARIO_ACTUAL es el usuario del alumno que clona el repositorio y USUARIO_PROPIETARIO es el dueño del repositorio.

Una vez hecho esto, utiliza la herramienta CMake vista en clase para generar el ejecutable. El fichero CMakeLists.txt se ofrece dentro del código.

## EJERCICIO POR PAREJAS ##

El objetivo de este ejercicio es añadir movimiento tanto a la pelota como a las raquetas. Para ello, el #alumno1 debe añadir el código necesario para mover la pelota (Esfera.cpp) y el #alumno2 para mover las raquetas (Raqueta.cpp).

Para realizar este ejercicio, el #alumno1 debe encargarse de hacer un "Fork" del original y el #alumno2 debe hacer un clone de este fork. El #alumno1 debe dar permisos de edición al #alumno2, desde la web del repositorio en Bitbucket (Settings -> Access Management).

El #alumno1 debe añadir un fichero Changelog donde indique un texto descriptivo de los cambios realizados en cada commit. Este fichero también debe ser modificado por el #alumno2 después de que lo suba el #alumno1.

El alumno debe familiarizarse con las opciones "status" y "log":

```
git status
git log  
```

Los alumnos deben marcar los ficheros cambiados mediante "git add" para añadirlos al siguiente commit. Una vez se hayan marcado los cambios, se realiza el "commit" local indicando un mensaje explicativo:

```
git commit -m 'Mensaje del commit'
```

A continuación, se debe subir el código al repositorio remoto:

```
git push origin master
```

El alumno debe actualizar su repositorio local con el commit más nuevo mediante "pull" (en este caso no debería haber conflictos):

```
git pull
```

Por último, uno de los alumnos debe crear una etiqueta (tag) para indicar el nuevo número de versión 1.1 y enviarla al servidor remoto, mediante los mandatos:

```
git tag -a v1.1 -m 'mensaje version 1.1'
git push origin v1.1 
```

Recuerda que puedes utilizar la interfaz gráfica de git y gitk:

```
gitk
git gui 
```

## EJERCICIO INDIVIDUAL PROPUESTO ##

Se pide completar el juego con alguna funcionalidad extra de las siguientes propuestas:

* Las raquetas deben ser capaces de disparan y reducir el tamaño de la raqueta oponente en el momento del impacto, o inmovilizarla unos segundos.
* La pelota debe disminuir el tamaño según avanza el juego.
* Duplicar el número de pelotas según avanza el juego.

Se pide modificar el archivo Changelog indicando las funcionalidades desarrolladas y un fichero README si fuera necesario indicar las instrucciones para probar el juego.

**El alumno debe subir todo el código a Bitbucket para ser evaluado por el profesor al comienzo de la práctica 2.**